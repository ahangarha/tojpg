#!/usr/bin/env python3

import sys
import subprocess
import re

args = sys.argv

if len(args) < 2:
    print("Not enough arguments!")
    exit(1)

# get files
args = args[1:]

for input_file in args:
    # get filename by excluding extension
    filename = re.findall(r"^(.+)\.[^\.]+$", input_file)[0]
    #new filename
    output_file = filename + ".jpg"
    command = "convert '{}' '{}'".format(input_file, output_file)
    subprocess.run(command, shell=True)
