# ToJPG

This is a simple script written in python 3 to make it easier to mass convert
image files into JPG format.

Notice that it needs `ImageMagick` to be installed on the system.

## How to use

Copy the file to a directory in which you system looks for executables. To use
is easier, you may remove `py` extension.

Run the scpipt by passing your files as argument, individually or by using
wildcard. Examples:

`$ tojpg input_file.png second_input_file.png`

or

`$ tojpg *.png`

Then, for each file, a new file will be created with `jpg` extension.

# Lisence

GNU General Public License v3
